# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.10.1"
  constraints = "~> 3.0"
  hashes = [
    "h1:OY98oWH09LJb4kD68kJNP+H+tEEhFkvx5WExq+GzKec=",
    "zh:0e4914c44984bdfc11140198950cbabe09fada993035f1cdf4a55b3e987ab3cc",
    "zh:1671345b9b0d6f65ed14242e7ff0f1792e6fc2fc4c1df1c1526818882c45b9af",
    "zh:1796f31caf0719b0bd65ad1b8ed8711b3942162e5c50a0bd4653332986b930cc",
    "zh:247c6d4a60f4bc595a11d323ea5c3720eb752c4db380e5a8be67c97c03744a22",
    "zh:479a4a1a332aa44aac706c59244c90d83dcaca08feacec81633c2316d1aba1df",
    "zh:53927fbf617a6234db12d74b0f970a65cca004ea13def2138b43286965d40e43",
    "zh:73d0277597b7ccec261538ee49b1ddb1118fe28cf626df43b5e09abbffe6aa5d",
    "zh:7431e3f2d0c254cc3f77bc62ad1eefb8ae7ee52d0de7b28ac90b6a98c67694ea",
    "zh:98d859f6e510ddbe819ffb1c1d6280876298645dd0f74d25f6ed320dc4772b95",
    "zh:c219569db8de5f7b5a402182599a32207dec0fef35c950c7594ceee3e95c4c94",
    "zh:c312da4780bf776e3ef18c9e719b0a2dd0656f047a7ef5058dda3a3a931bb8d8",
    "zh:c752fa6860b3ccbec0ee359cfad805d6c6205366c5889341dc6ee6cb6bef0bdd",
    "zh:cd9f3d4f480a262d3e4eb663fcdfbc788ea6d8063beca19eed26e962c2df3725",
    "zh:f569573661f68e72e9044b8cf76ee40f652b19c788c0e26baf52bfbf84c9f26f",
  ]
}

provider "registry.terraform.io/digitalocean/digitalocean" {
  version = "2.18.0"
  hashes = [
    "h1:Azh91wxXTHzalGnI3HHuNBx4THFxqPb/b7BkkIV3A4o=",
    "zh:2c5322ea6de0aff88dd5c19634bc01c1907a0777d926149ea3f86bf3f2047ff8",
    "zh:4d3a363d5d16362756042f9461a9bb68c6ddd45d16f7da972d696fa3a1d03d5d",
    "zh:5fc0374435e01d9b8a87351ab91ac384464a71f083ec1d59342da15ffadeb1a6",
    "zh:6e07f148cf0820d8780d2b5569d7c1817f546bc0a2757d6b42c112f3f8f8d46c",
    "zh:705326caa2cdf5e4a370cdc27fd29be380c207e4e6c8a411e5494af1155817dc",
    "zh:8f36faacfa2013750ede964577f9d5c273929ad43b082ca4e31641260f8b5730",
    "zh:a10e20d534ee12ea8a8aceeb3a96e0d946a511f4981d7ce5bbf479aaff8768ba",
    "zh:b23d21b59e174a2f02ee1aa95b9cff9f88da0ac2f42765ed6be2b8891cbff7e9",
    "zh:be9bd194fadcae235910ac08c90d6359a8a51dd76b0897ab3475d1b08e6a50b9",
    "zh:c093148fbeacddc7b7e08c2c015e413a4ec4805d07349d06e51162460445c05c",
    "zh:cc3e7b6d21f652f14919a7338aae59e4f181f64583d311f32b440361b933b05b",
    "zh:ea0096068f2b4c7b11a954469b7f9823cbd6670f92837cd76b0716a3fba83b71",
    "zh:eeb18c2d2ef7cd95ec0d7b7a57f3d2e0d91de29931aaa44ab9588689695723e2",
    "zh:f883ba115683a2f126ec78aca3bea6c7aca0c4a8a316f44129dbb5cdc798d46a",
    "zh:f8aa7e15c90aa231532ed5f2d809acbaf4f3bcff24bb040e185e64cca541f99b",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:0wlehNaxBX7GJQnPfQwTNvvAf38Jm0Nv7ssKGMaG6Og=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}
