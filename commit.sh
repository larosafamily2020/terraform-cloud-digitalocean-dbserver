#!/usr/bin/env bash
#THEHOME="$(sudo su root -c '$HOME')"

#sudo docker exec mysql /usr/bin/mysqldump -u dbuser_sa --password=newpassword5 ejs > /terraform-cloud-digitalocean-dbserver/ejs.sql
#wait $BACK_PID
#sudo chown -R root:root /terraform-cloud-digitalocean-dbserver/db-backup
#echo "Backup complete"

BASE="/terraform-cloud-digitalocean-dbserver"
cd $BASE
git log -n 1 --pretty=format:'%h : %s' --graph > commit_dump.txt
echo "$(cat commit_dump.txt | cut -f4 -d " ")" > commit.txt
git add commit.txt
git commit -m "DEVOPS-40 Updated commit file"
git push origin blue
