#! /bin/bash
database=mysql
password=root

echo "----------- check mysql version -------------"
mysql -uroot -p$password $database -e "show version() from dual;"
echo "----------- check mysql variables(global and session) -------------"
mysql -uroot -p$password $database -e "show variables;"
echo "----------- check mysql current user processes -------------"
mysql -uroot -p$password $database -e "show processlist;"
echo "----------- check mysql server status -------------"
mysql -uroot -p$password $database -e "show status;"
echo "----------- check mysql innodb engine status -------------"
mysql -uroot -p$password $database -e "show engine innodb status;"
echo "----------- check mysql database table data size, row num, index size-------------"
mysql -uroot -p$password $database -e "SELECT CONCAT(table_schema,'.',table_name) AS 'Table Name', CONCAT(ROUND(table_rows/1000000,4),'M') AS 'Number of Rows', CONCAT(ROUND(data_length/(1024*1024*1024),4),'G') AS 'Data Size', CONCAT(ROUND(index_length/(1024*1024*1024),4),'G') AS 'Index Size', CONCAT(ROUND((data_length+index_length)/(1024*1024*1024),4),'G') AS'Total'FROM information_schema.TABLES;"
