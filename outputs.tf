output "ip_address" {
  value = digitalocean_droplet.generic_name.ipv4_address
  description = "The public IP address of your Droplet application."
}

output "Region" {
  value = var.NEXT_REGION
  description = "The region of your Droplet application."
}
output "Hostname" {
  value = var.NEXT_HOSTNAME
  description = "The hostname of your Droplet application."
}
output "fqdn" {
  value = var.NEXT_FQDN
  description = "The zone of your Droplet application."
}

data "local_file" "commits" {
    filename = "commit.txt"
}
output "commit" {
  value = chomp(data.local_file.commits.content)
  description = "The Latest commit"
}
output "Date" {
   value =  timestamp()

}
