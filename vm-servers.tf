# Root SSH Key
resource "digitalocean_ssh_key" "root" {
  name       = "ROOT Public Key"
  public_key = "${var.ROOT_PUBLIC_KEY}"
}
resource "digitalocean_droplet" "generic_name" {
  image = "centos-7-x64"
  name = "${var.NEXT_DROPLET_NAME}"
  region =  "${var.NEXT_REGION}"
  size = "s-1vcpu-2gb"
  ssh_keys = [
    digitalocean_ssh_key.root.fingerprint
  ]
  connection {
    type     = "ssh"
    user     = "root"
    private_key = "${var.ROOT_PRIVATE_KEY}"
    timeout  = "2m"
    host = "${digitalocean_droplet.generic_name.ipv4_address}"
  }
  
  provisioner "file" {
    source      = "progress_checker.sh"
    destination = "/tmp/progress_checker.sh"
  }
  provisioner "file" {
    source      = "nginx.conf"
    destination = "/tmp/nginx.conf"
  }
  
  provisioner "file" {
    source      = "node_exporter.service"
    destination = "/tmp/node_exporter.service"
  }
  provisioner "file" {
    source      = "my.cnf"
    destination = "/tmp/my.cnf"
  }
  
  provisioner "file" {
    source      = "commit.txt"
    destination = "/tmp/commit.txt"
  }

  provisioner "file" {
    source      = "insert.py"
    destination = "/tmp/insert.py"
  }
  provisioner "remote-exec" {
    inline = [
      "echo '${var.ROOT_PUBLIC_KEY}' >> ~/.ssh/authorized_keys",
      "echo \"-----BEGIN OPENSSH PRIVATE KEY-----\" > ~/.ssh/id_rsa",
      "echo '${var.id_db1_private_key}' >> ~/.ssh/id_rsa",
      "echo \"-----END OPENSSH PRIVATE KEY-----\" >> ~/.ssh/id_rsa",
      "chmod 0600 ~/.ssh/id_rsa",
      "mkdir /tmp/ssl",
      "echo \"-----BEGIN CERTIFICATE-----\" > /tmp/ssl/cert.pem",
      "echo '${var.NGINX_SSL_CERT}' >> /tmp/ssl/cert.pem",
      "echo \"-----END CERTIFICATE-----\" >> /tmp/ssl/cert.pem",
      "echo '-----BEGIN PRIVATE KEY-----' | sudo tee /tmp/ssl/key.pem > /dev/null",
      "echo '${var.NGINX_SSL_KEY}' >> /tmp/ssl/key.pem",
      "echo '-----END PRIVATE KEY-----' >> /tmp/ssl/key.pem",
      "mkdir /tmp/mysql_certs",
      "echo '-----BEGIN CERTIFICATE-----' > /tmp/mysql_certs/ca.pem",
      "echo '${var.MYSQL_CA}' >> /tmp/mysql_certs/ca.pem",
      "echo '-----END CERTIFICATE-----' >> /tmp/mysql_certs/ca.pem",
      "echo '-----BEGIN CERTIFICATE-----' > /tmp/mysql_certs/server-cert.pem",
      "echo '${var.MYSQL_SERVER_CERT}' >> /tmp/mysql_certs/server-cert.pem",
      "echo '-----END CERTIFICATE-----' >> /tmp/mysql_certs/server-cert.pem",
      "echo '-----BEGIN RSA PRIVATE KEY-----' > /tmp/mysql_certs/server-key.pem",
      "echo '${var.MYSQL_SERVER_KEY}' >> /tmp/mysql_certs/server-key.pem",
      "echo '-----END RSA PRIVATE KEY-----' >> /tmp/mysql_certs/server-key.pem",
      "chmod 0444 /tmp/mysql_certs/ca.pem && chmod 0400 /tmp/mysql_certs/server-key.pem && chmod 0444 /tmp/mysql_certs/server-cert.pem",
      "sh /tmp/progress_checker.sh",
    ]
  }
  user_data = data.template_file.cloud-init-yaml.rendered
}
data "cloudflare_zone" "generic_name" {
  name = "${var.NEXT_ZONE}"
}
resource "cloudflare_record" "generic_name" {
  zone_id   = data.cloudflare_zone.generic_name.id
  name = "${var.NEXT_HOSTNAME}"
  value   = "${digitalocean_droplet.generic_name.ipv4_address}"
  type    = "A"
  ttl     = 300
  proxied = false
}
data "template_file" "cloud-init-yaml" {
  template = file("${path.module}/cloud-init.yaml")
  vars = {
    NEXT_VC_BRANCH = "${var.NEXT_VC_BRANCH}",
    NEXT_FQDN = "${var.NEXT_FQDN}",
    DBUSER_SA_PASS = "${var.DBUSER_SA_PASS}",
    NGINX_SSL_CERT = "${var.NGINX_SSL_CERT}",
    NGINX_SSL_KEY = "${var.NGINX_SSL_KEY}",
    MYSQL_CA = "${var.MYSQL_CA}",
    MYSQL_SERVER_CERT = "${var.MYSQL_SERVER_CERT}",
    MYSQL_SERVER_KEY = "${var.MYSQL_SERVER_KEY}",
    MY_GIT_PRV_KEY = "${var.id_db1_private_key}",
    GIT_USER = "${var.NEXT_GIT_USER}"
  }
}
